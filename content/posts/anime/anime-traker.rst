Scelta di un anime tracker
##########################

:date: 2019-07-25
:category: anime
:slug: anime-tracker
:authors: Fede
:status: draft
:lang: it

Ho appena iniziato con la scelta di un anime tracker, m voi vi chiederete: cos'è un anime tracker?

Un anime tracker è un servizio che permette di tenere traccia di tutti gli anime e i manga che si sono guardati, che si stanno guardando e che si vorranno guardare.

Io personalmente ho provato solo 3 di questi servizi: Kitsu, MyAnimeList e Anilist.

Kitsu: è molto organizzato, ha un' applicazione apposita (che però va abbastanza lenta), si può personalizzare moltissimo il profilo ma non l'interfaccia (che però possiede un colore gradevole sia di giorno che di notte) e inoltre possiede la sincronizzazione a MyAnimeList

MyAnimeList: abbreviato MAL, è il tracker più diffuso anche se, a mio parere e più complicato degli altri. Possiede una grandissima personalizzazione dell'interfaccia (che però può sembrare non molto moderna rispetto agli altri servizi) e del profilo. Per mobile esistono molti client non ufficiali che però funzionano egregiamente.

Anilist: stile moderno e material, si può personalizzare il colore del sito ed è molto simile a Kitsu, anche se per mobile esistono solo client non ufficiali.
Io sto usando Kitsu e mi trovo molto bene, fatemi sapere voi cosa usate, Fede