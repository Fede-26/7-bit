The choice of an anime tracker
##############################

:date: 2019-07-25
:category: anime
:slug: anime-tracker
:authors: Fede
:status: draft
:lang: en

I just started with the choice of an anime tracker, but you may wonder: what is an anime tracker?

An anime tracker is a service that allows you to keep track of all the anime and manga that you have watched, that you are watching and that you will want to watch.

I personally have only tried 3 of these services: Kitsu, MyAnimeList and Anilist.

Kitsu: it is very organized, it has a special application (which, however, is quite slow), you can very much customize the profile but not the interface (which, however, has a pleasant color both day and night) and also has synchronization to MyAnimeList

MyAnimeList: abbreviated MAL, it is the most widespread tracker although, in my opinion and more complicated than the others. It has a great customization of the interface (which may seem not very modern compared to other services) and of the profile. There are many unofficial clients for mobile but they work very well.

Anilist: modern style and material, the color of the site can be customized and is very similar to Kitsu, even if there are only unofficial clients for mobile.
I am using Kitsu and I am very well, let me know what you use, Fede