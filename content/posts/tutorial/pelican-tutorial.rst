Creare un sito con pelican
##########################

:date: 2019-08-20
:category: tutorial
:slug: pelican-tutorial
:tags: pelican, tutorial
:authors: Fede
:status: published
:lang: it
:summary: Avete sempre sognato di creare un blog tutto vostro per condividere idee, pensieri e conoscenze?

Avete sempre sognato di creare un blog tutto vostro per condividere idee, pensieri e conoscenze?
Se la risposta è no allora potete anche guardare altri post di questo sito, altrimenti continuate la lettura.

Questa guida deriva dalla `documentazione originale di Pelican <https://docs.getpelican.com/>`_.

~~~~

Capire cosa volete realmente
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Questa guida è stata scritta per aiutare nella creazione di un sito statico (ovvero come questo, che non presenta funzioni dinamiche) con `Pelican <https://blog.getpelican.com/>`_.

Se si vuole per caso creare un sito di chat, forum, ... dovete cercarvi un altro tutorial.

Il sito web verra hostato su `Gitlab Pages <https://about.gitlab.com/product/pages/>`_, che è gratuito, ma verra spiegato nella seconda parte di questa guida.

~~~~

Requisiti
^^^^^^^^^
 - Un computer con Linux
 - Git
 - Python 3
 - pip3
 - make
 - `virtualenv <http://www.virtualenv.org/>`_ (raccomandato)
 - Conoscenza base di python (raccomandato)


Installazione
^^^^^^^^^^^^^
Per creare un `virtualenv <http://www.virtualenv.org/>`_ per pelican digitare nel terminale linux:

.. code-block:: bash

 virtualenv ~/virtualenvs/pelican
 cd ~/virtualenvs/pelican
 source bin/activate

dopodiché installare Pelican con pip3:

.. code-block:: bash

 pip3 install pelican
	
se non funziona riprovare con:

.. code-block:: bash

	sudo pip3 install pelican


Se si decide di usare `Markdown <http://pypi.python.org/pypi/Markdown>`_ come linguaggio per i post al posto di rst (restructuredtext, consigliato) bisogna installarlo con:

.. code-block:: bash

	pip3 install Markdown
	

Quickstart
^^^^^^^^^^
Per creare il proprio sito posizionarsi nella cartella dove verra creato il progetto e digitare:

.. code-block:: bash

	pelican-quickstart
	
dopo aver risposto ad alcune domande si formeranno le cartelle del progetto

.. code-block:: text

	progetto/
	├── content
	│   └── (pages)
	├── output
	├── tasks.py
	├── Makefile
	├── pelicanconf.py
	└── publishconf.py


- La cartella pages è opzionale, si usa per pagine non cronologiche (come Contatti o About)
- pelicanconf.py è il file di configurazione principale
- publishconf.py è il file di configurazione usato quando il sito viene pubblicato (contiene pelicanconf.py)


Metadati dei file
^^^^^^^^^^^^^^^^^
Pelican cerca di essere abbastanza intelligente da ottenere le informazioni di cui ha bisogno dal file system (ad esempio, sulla categoria dei tuoi articoli), ma alcune informazioni che devi fornire sotto forma di metadati all'interno dei tuoi file.

Se stai scrivendo il tuo contenuto in formato reStructuredText, puoi fornire questi metadati in file di testo tramite la sintassi seguente (assegna al tuo file l'estensione .rst):

.. code-block:: rst

 Il mio super titolo
 ##############

 :date: 2010-10-03 10:20
 :modified: 2010-10-04 18:40
 :tags: fantastico
 :category: yeah
 :slug: my-super-post
 :authors: Alexis Metaireau, Conan Doyle
 :summary: versione breve per indice e fee

Gli elenchi di autori e tag possono essere separati da punto e virgola, il che consente di scrivere autori e tag contenenti virgole:

.. code-block:: rs

 :tags: pellicano, strumento di pubblicazione;  pellicano, uccello
 :authors: Metaireau, Alexis;  Doyle, Conan


Di seguito è riportato un elenco di parole chiave riservate per i metadati:

- Title
- Tags
- Date
- Modified
- Status
- Category
- Author
- Author
- Slug
- Summar
- Template
- Save_as
- Url
 
Nota che, a parte il titolo, nessuno di questi metadati del contenuto è obbligatorio: se la data non è specificata e DEFAULT_DATE è impostato su 'fs', Pelican si affiderà al timestamp "mtime" del file e la categoria può essere determinata dal  directory in cui risiede il file.  Ad esempio, un file situato in python/foobar/myfoobar.rst avrà una categoria di foobar.  Se desideri organizzare i tuoi file in altri modi in cui il nome della sottocartella non sia un buon nome di categoria, puoi impostare la variabile USE_FOLDER_AS_CATEGORY su False.

Quindi il titolo è l'unico metadato richiesto.  Se questo ti disturba, non preoccuparti.  Invece di specificare manualmente un titolo nei metadati ogni volta, è possibile utilizzare il nome del file di contenuto di origine come titolo.  Ad esempio, a un file sorgente chiamato Publishing via Pelican.rst verrebbe automaticamente assegnato un titolo di Publishing via Pelican.  Se preferisci questo comportamento, aggiungi la seguente riga al tuo file delle impostazioni:

.. code-block:: python

	FILENAME_METADATA = '(? P <title>. *)'
	
Il metadato modified dovrebbe essere l'ultima volta che hai aggiornato l'articolo e, se non specificato, per impostazione predefinita è impostato fino ad oggi. Inoltre puoi mostrare le modifiche nei modelli, le voci dei feed nei lettori di feed verranno aggiornate automaticamente quando imposti le modifiche alla data corrente dopo aver modificato l'articolo.

authors è un elenco separato da virgole di autori di articoli.  Se esiste un solo autore, puoi utilizzare il campo author.

Se non specifichi esplicitamente il metadato summary per un determinato post, puoi utilizzare l'impostazione SUMMARY_MAX_LENGTH per specificare quante parole dall'inizio di un articolo vengono utilizzate come riepilogo.

Puoi anche estrarre qualsiasi metadato dal nome file tramite un'espressione regolare da impostare nel file di configurazione FILENAME_METADATA.  Tutti i gruppi con nome corrispondenti verranno impostati nell'oggetto metadati.  Il valore predefinito per l'impostazione FILENAME_METADATA estrarrà solo la data dal nome file.  Ad esempio, se desideri estrarre sia la data che lo slug, potresti impostare qualcosa del tipo: 

.. code-block:: python

 FILENAME_METADATA = '(? P <data> \ d {4} - \ d {2} - \ d {2}) _ (?  P <slug>. *)'

Nota che i metadati disponibili all'interno dei tuoi file hanno la precedenza sui metadati estratti dal nome file.


Genera il tuo sito
^^^^^^^^^^^^^^^^^^
Dalla directory del tuo sito, esegui il comando pelican per generare il tuo sito:

.. code-block:: bash

	pelican content

Il tuo sito è stato ora generato all'interno della directory di output (public). (È possibile che venga visualizzato un avviso relativo ai feed, ma è normale quando si sviluppa in locale e può essere ignorato per ora)


Visualizza l'anteprima del tuo sito
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Apri una nuova sessione terminale, vai alla directory di output generata ed esegui il comando seguente per avviare il web server Pelican:

.. code-block:: bash

	pelican --listen

Visualizza l'anteprima del tuo sito navigando su http://localhost:8000/ nel tuo browser.