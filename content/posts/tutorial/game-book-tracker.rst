Creare un script per i game-book
################################

:date: 2019-08-26
:category: tutorial
:slug: game-book-tracker
:tags: game-book, python, script
:authors: Fede
:status: draft
:lang: It
:summary: Creare un programma in python per automatizzare gli scontri nei game-book

Ho creato questo programma per aiutarmi negli scontri che avvengono nel game-book.
Utilizza Pickle per serializzare i dati in un file, in questo modo anche uscendo dal programma i dati vengono salvati.

~~~~

Creare il file di Pickle
^^^^^^^^^^^^^^^^^^^^^^^^
Per prima cosa bisogna inizializzare i dati per la prima volta e per questo si usa quest'altro script:

.. code-block:: python

	#use this to initialize the variable with Pickle
    import pickle

    diary = {}

    diary["comb"] = int(input("Combattività: "))
    diary["res"] = int(input("Resistenza: "))
    diary["res_max"] = int(input("Resistenza massima: "))
    diary["armi"] = [input("arma 1: ").lower(), input("arma 2:  ").lower()]
    diary["corone"] = int(input("Corone d'oro: "))
    diary["pasti"] = int(input("Pasti: "))

    with open('diary_data.pkl', 'wb') as f:           #save file with Pickle
        pickle.dump(diary, f)

    print(diary)

Analizziamo il programma:

.. code-block:: python

    import pickle

Bisogna importare il modulo Pickle.

.. code-block:: python

    diary = {}

Dopodiche bisogna inizializzare il dizionario 'diary'.

.. code-block:: python

    diary["comb"] = int(input("Combattività: "))
    diary["res"] = int(input("Resistenza: "))
    diary["res_max"] = int(input("Resistenza massima: "))

Vengono inizializzate le variabili 'comb' (combattività), 'res' (resistenza) e 'res_max' (resistenza massima) sotto forma di integer (interi).

.. code-block:: python

    diary["armi"] = [input("arma 1: ").lower(), input("arma 2:  ").lower()]

Nel caso delle armi in possesso (massimo 2) bisogna creare una lista 'armi' con 2 item.

.. code-block:: python

    diary["corone"] = int(input("Corone d'oro: "))
    diary["pasti"] = int(input("Pasti: "))

Vengono inizializzate le ultime variabili.

.. code-block:: python

    with open('diary_data.pkl', 'wb') as f:           #save file with Pickle
        pickle.dump(diary, f)

Il file 'diary_data.pkl' viene salvato con all'interno le variabili.

    Da notare che Pickle non salva le variabili con un formato leggibile, ma sotto forma di byte.
    Quindi se si volesse leggere il contenuto del file bisognerebbe leggerlo tramite Pickle stesso.

.. code-block:: python

    print(diary)

Per verificare che il contenuto del dizionario sia giusto.

~~~~

tracker.py
^^^^^^^^^^
Ed ora lo script vero e proprio:

.. code-block:: python

    import pickle
    import random

    with open('diary_data.pkl', "rb") as f:  # load the diary with pickle
        diary = pickle.load(f)

    print(diary, "\n")

    looping = True
    while looping:

        choose = int(input("\nCosa vuoi fare:\n 1. guardare le statistiche\n 2. modificare le statistiche\n 3. combattere\n 99. uscire\n< "))

        if choose == 1:

            for key, value in diary.items():
                print("{}: {}".format(key, value))
            print("\n")

        elif choose == 2:

            for key, value in diary.items():
                need_to_modify = input("{} è attualmente {}, modificare il valore ? (y/N): ".format(key, value))

                if need_to_modify.lower() == "y":
                    if key == "armi":
                        new_value = [input("nuovo valore 1: "),input("nuovo valore 2: ")]

                    else:
                        new_value = int(input("nuovo valore: "))

                    diary[key] = new_value

        elif choose == 3:

            enemy_comb = int(input("< La comb. del nemico: "))
            enemy_res = int(input("< La res. del nemico: "))
            is_enemy_death = False
            while not is_enemy_death:
                self_comb = diary["comb"]
                print("> La tua comb.:", self_comb)
                bonus_comb = input("< Modifiche alla tua comb.: ")
                exec("self_comb = self_comb" + bonus_comb)
                print("> La tua comb. complessiva:", self_comb)
                rapp_comb = self_comb - enemy_comb
                print("> Il rapp. comb. è {}, il numero estratto è {}".format(rapp_comb, random.randint(0, 9)))
                res_loss = int(input("< Quanta vita hai perso? : "))
                diary["res"] -= res_loss

                if diary["res"] <= 0:
                    print("\n-- YOU ARE DEATH --\n")
                    exit()

                enemy_res_loss = int(input("< Quanta vita ha perso il nemico? : "))
                enemy_res -= enemy_res_loss

                if enemy_res <= 0:
                    print("\n-- IL NEMICO è MORTO --\n".upper())
                    print("> La tua res.:", diary["res"])
                    is_enemy_death = True

        elif choose == 99:

            print("\n\n> exiting\n")
            looping = False

Analizziamolo:

.. code-block:: python

    import pickle
    import random

Importa i moduli.

.. code-block:: python

    with open('diary_data.pkl', "rb") as f:  # load the diary with pickle
        diary = pickle.load(f)

    print(diary, "\n")

Carica il dizionario 'diary' e lo stampa.

.. code-block:: python

    looping = True
    while looping:

        choose = int(input("\nCosa vuoi fare:\n 1. guardare le statistiche\n 2. modificare le statistiche\n 3. combattere\n 99. uscire\n< "))

Crea un loop che si ripete finchè non si esce e chiede cosa si vuole fare:

.. code-block:: python

        if choose == 1:

            for key, value in diary.items():
                print("{}: {}".format(key, value))
            print("\n")

Se si sceglie '1', vengono stampate tutte le variabili contenute nel dizionario 'diary';

.. code-block:: python

        elif choose == 2:

            for key, value in diary.items():
                need_to_modify = input("{} è attualmente {}, modificare il valore ? (y/N): ".format(key, value))

                if need_to_modify.lower() == "y":
                    if key == "armi":
                        new_value = [input("nuovo valore 1: "),input("nuovo valore 2: ")]

                    else:
                        new_value = int(input("nuovo valore: "))

                    diary[key] = new_value

Se invece si sceglie '2', per ogni variabile in 'diary' viene chiesto se bisogna modificarlo e se si, viene chisto il valore di essa;

.. code-block:: python

        elif choose == 3:

            enemy_comb = int(input("< La comb. del nemico: "))
            enemy_res = int(input("< La res. del nemico: "))
            is_enemy_death = False
            while not is_enemy_death:
                self_comb = diary["comb"]
                print("> La tua comb.:", self_comb)
                bonus_comb = input("< Modifiche alla tua comb.: ")
                exec("self_comb = self_comb" + bonus_comb)
                print("> La tua comb. complessiva:", self_comb)
                rapp_comb = self_comb - enemy_comb
                print("> Il rapp. comb. è {}, il numero estratto è {}".format(rapp_comb, random.randint(0, 9)))
                res_loss = int(input("< Quanta vita hai perso? : "))
                diary["res"] -= res_loss

                if diary["res"] <= 0:
                    print("\n-- YOU ARE DEATH --\n")
                    exit()

                enemy_res_loss = int(input("< Quanta vita ha perso il nemico? : "))
                enemy_res -= enemy_res_loss

                if enemy_res <= 0:
                    print("\n-- IL NEMICO è MORTO --\n".upper())
                    print("> La tua res.:", diary["res"])
                    is_enemy_death = True

Se invece si sceglie '3' vengono chieste le informazioni sul nemico, viene calcolato il rapporto di combattività e viene esratto un numero da 0 a 9.
Dopo aver inserito i danni subiti e i danni effettuati viene ricalcolata la resistenza di entrambi e se una delle due arriva a zero viene comunicato.

.. code-block:: python

        elif choose == 99:

            print("\n\n> exiting\n")
            looping = False

Se invece si sceglie '99' il loop viene interrotto e si esce dal programma.

~~~~

Questo programma viene distribuito sotto licenza GPLv3