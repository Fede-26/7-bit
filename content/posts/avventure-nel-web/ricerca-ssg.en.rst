My search for an ssg
####################

:date: 2019-06-26
:tags: ssg
:category: web adventures
:slug: ricerca-ssg
:authors: Fede
:status: published
:lang: en

Today I want to tell you about my adventure in finding a static site generator.
The one used for this site is called Pelican and is based on python and uses Jinja for templates.
But it was not the first tested.
Before I even learned about these frameworks, I had tried some free hosting sites that used a Drag and Drop interface (Altervista, Wix, ..).
The only problem was that in addition to being slow, many features were only available for a fee and therefore I was very limited in use.
After a while, rearranging my folders on Gdrive, in the new file function I saw that not only were the modules or drawings available. but also the sites.
Just tried google sites became my service of creation and primary hosting because it was free, it had many functions (even if a little limited), above all it allowed to have all the sites you wanted.
I created my first site: Caffeine lunch, now discontinued (or maybe not ..).
Visiting some sites, especially developers, I noticed that even on github and gitlab (now the most frequently used git server) sites could be created.
I inquired and discovered that only static sites could be created (perfect, since I only wanted to create a small blog) but that had to be created from scratch.
Only later, looking at other users of the same platform, I realized that there were simpler ways: a framework for generating static sites.
At first I tried Hugo: written in Go I started using it but I didn't like how it was organized, but it had a theme similar to a terminal that I would still like to use.
Then I tried Jekyll: written in Ruby, but as soon as I tried to install bundler and various gems began to give me errors on errors, my attempts to make it work were useless.
Now I'm testing Pelican, which being written in python I can more or less understand it.
We hope that everything works for the best, Fede
