La mia ricerca di un ssg
########################

:date: 2019-06-26
:tags: ssg
:category: avventure nel web
:slug: ricerca-ssg
:authors: Fede
:status: published
:lang: it

Oggi vi voglio raccontare una mia avventura nella ricerca di un generatore di siti statici.
Quello usato per questo sito si chiama Pelican ed è basato su python ed utilizza Jinja per i template.
Ma non è stato il primo testato.
Prima ancora di venire a conoscenza di questi framework, avevo provato alcuni siti di hosting gratuito che utilizzavano un'interfaccia Drag and Drop (Altervista, Wix, ..).
L'unico problema era che oltre ad essere lenti, molte funzioni erano disponibili solo a pagamento e quindi ero molto limitato nell'uso.

Dopo poco, riordinando le mie cartelle su Gdrive, nella funzione nuovo file vidi che non solo erano disponibili i moduli o i disegni. ma anche i siti.
Appena provato google sites divenne il mio servizio di creazione e hosting primario perchè era gratuito, aveva molte funzioni (anche se un po' limitate), mma soprattutto permetteva di avere tutti i siti che volevi.

Creai il mio primo sito: Caffeine lunch, ormai discontinuato (o forse no ..).

Visitando alcuni siti, soprattutto di sviluppatori notai che anche su github e gitlab (ormai il server git che uso con più frequenza) si potevano creare siti.

Mi informai e scoprii che si potevano creare soltanto siti statici (cosa perfetta, visto che volevo solo creare un piccolo blog) ma che però bisognava crearli da zero.
Solo dopo, guardando altri utenti della stessa piattaforma capii che esistevano modi più semplici: un framework per la generazione di siti statici.

All'inizio provai Hugo: scritto in Go lo iniziai ad usare ma non mi piaceva come era organizzato, ma aveva un tema simile ad un terminale che vorrei ancora usare.
Poi provai Jekyll: scritto in Ruby, ma appena provai ad installare bundler e varie gems cominciò a darmi errori su errori, inutili erano i miei tentativi di farlo funzionare.
Ora sto testando Pelican, che essendo scritto in python riesco più o meno a capirlo.

Speriamo che funzioni tutto per il meglio, Fede
