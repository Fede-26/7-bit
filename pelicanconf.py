#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Fede_26'
SITENAME = '7 BIT'
SITESUBTITLE = 'a blog a bit off'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'it'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),())

# Social widget
SOCIAL = (('Github', 'https://github.com/Fede-26'),
          ('Gitlab', 'https://gitlab.com/Fede-26'),
          ('devRant', 'https://devrant.com/users/Fede26'),
          #('Kitsu', 'https://www.kitsu.io/users/Fede_26'),
          ('IRC channel', 'https://kiwiirc.com/client/irc.freenode.net/#7bit'),
          ('Repository', 'https://gitlab.com/Fede-26/fede-26.gitlab.io'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = [
    'images',
    'extra',  # this
]
EXTRA_PATH_METADATA = {
    #    'extra/custom.css': {'path': 'custom.css'},
    #    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'},  # and this
    #    'extra/CNAME': {'path': 'CNAME'},
    #    'extra/LICENSE': {'path': 'LICENSE'},
    #    'extra/README': {'path': 'README'},
}

ARTICLE_PATHS = ['posts']
USE_FOLDER_AS_CATEGORY = True

THEME = "pelican-themes/gum"

PLUGIN_PATHS = ["pelican-plugins"]
PLUGINS = ["i18n_subsites","video_privacy_enhancer"]

# mapping: language_code -> settings_overrides_dict
I18N_SUBSITES = {
    'en': {
        #        'SITENAME': 'Hezkej blog',
    }
}

TYPOGRIFY = True
SLUGIFY_SOURCE = 'basename'