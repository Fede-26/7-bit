#!/usr/bin/env python
# -*- coding: utf-8 -*- #
#blog post maker
import os
print("\nmake a new blog post\n".upper())

#variable declaration
global command

#variable input  --start--
title = input("Title: ")
date = os.popen("date '+%Y-%m-%d'").read()
category = input("Category: ").lower()
slug = input("Slug: ").lower()
tags = input("Tags: ")
authors = input("Authors: ").capitalize()
if authors == "":
    authors = "Fede"
status = "draft"
lang = (input("Language: ").lower()).capitalize()
if(input("Add summary (y/N): ").lower() == "y"):
    add_summary = True
else:
    add_summary = False
#variable input  --end--
print("\n")
#path declaration  --start--

filename = slug + ".rst"
filedirectory = "./content/posts/" + category + "/"
pathfile = filedirectory + filename
print("File: " + pathfile + "\n")

list_catfolder = os.popen("ls content/posts/").read()           #list all the category (folder)

exist_category = category in list_catfolder                     #if the category exists
exist_file = False                                              #in case that the category doesn't exist

if exist_category:
    list_file = os.popen("ls " + filedirectory).read()          #list all of the file in th filedirectory
    exist_file = ("\n" + filename) in list_file                          #if the file (*.rst) already exists

#path declaration  --end--
#creating file  --start--

if exist_category:
    if exist_file:
        command = ""
    else:
        command = "touch " + pathfile
else:
    command = "mkdir " + filedirectory + " && " + "touch " + pathfile

print("DEBUG -- Command: " + command)
os.system(command)                          #execute the command

#creating file  --end--
#changing the variable to rst standards  --start--

title = title + "\n" + ("#" * len(title)) + "\n\n"
date = ":date: " + date# + "\n"
category = ":category: " + category + "\n"
slug = ":slug: " + slug + "\n"
tags = ":tags: " + tags + "\n"
authors = ":authors: " + authors + "\n"
status = ":status: " + status + "\n"
lang = ":lang: " + lang + "\n"

if add_summary:
    summary = ":summary: "
else:
    summary = ""

content = title + date + category + slug + tags + authors + status + lang + summary
#changing the variable to rst standards  --end--
#writing in the file  --start--

post_file = open(pathfile, "w")
post_file.write(content)
post_file.close()
print("DEBUG -- File writed")
#writing in the file --end--
#open editor --start--

#editor = "vscodium"
#open_editor = input("Do you want to open the editor (y/N): ").lower()
#
#if open_editor == "y":
#    open_editor = True
#    os.system(editor + " .")
#else:
#    open_editor = False


#open editor --end--


#print("\nhend")
hend = input("\npremere un tasto per continuare . . . ")
