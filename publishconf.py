#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://7bit.ga'
RELATIVE_URLS = False
SITENAME = '7 BIT'
SITESUBTITLE = 'a blog a bit off'


FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""

DEFAULT_LANG = 'it'

OUTPUT_PATH = 'public/'
THEME = '/data/pelican-themes/gum'
PLUGIN_PATHS = ['/data/pelican-plugins']
PLUGINS = ["i18n_subsites","video_privacy_enhancer"]

# mapping: language_code -> settings_overrides_dict
I18N_SUBSITES = {
    'en': {
#        'SITENAME': 'Hezkej blog',
        }
    }

STATIC_PATHS = [
    '/images',
    '/extra',  # this
]
EXTRA_PATH_METADATA = {
#    'extra/custom.css': {'path': 'custom.css'},
#    'extra/robots.txt': {'path': 'robots.txt'},
    '/extra/favicon.ico': {'path': 'favicon.ico'},  # and this
#    'extra/CNAME': {'path': 'CNAME'},
#    'extra/LICENSE': {'path': 'LICENSE'},
#    'extra/README': {'path': 'README'},
}

TYPOGRIFY = True
SLUGIFY_SOURCE = 'basename'